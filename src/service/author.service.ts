import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Author, AuthorDocument } from '../schema/author.schema';
import { AuthorInterface } from '../interface/author.interface';

@Injectable()
export class AuthorService {
  constructor(
    @InjectModel(Author.name)
    private readonly authorModel: mongoose.Model<AuthorDocument>,
  ) {}

  async findAll(): Promise<Author[]> {
    return this.authorModel.find().exec();
  }

  async findById(id: mongoose.Schema.Types.ObjectId): Promise<Author> {
    return this.authorModel.findById(id).exec();
  }

  async create(author: AuthorInterface): Promise<Author> {
    const createdAuthor = new this.authorModel(author);
    return createdAuthor.save();
  }

  async update(
    id: mongoose.Schema.Types.ObjectId,
    author: AuthorInterface,
  ): Promise<Author> {
    await this.authorModel.findByIdAndUpdate(id, author).exec();
    return this.findById(id);
  }

  async delete(id: mongoose.Schema.Types.ObjectId) {
    await this.authorModel.findByIdAndRemove(id).exec();
  }
}
