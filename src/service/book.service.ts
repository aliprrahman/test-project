import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Book, BookDocument } from '../schema/book.schema';
import { BookInterface } from '../interface/book.interface';

@Injectable()
export class BookService {
  constructor(
    @InjectModel(Book.name)
    private readonly bookModel: mongoose.Model<BookDocument>,
  ) {}

  async findAll(): Promise<Book[]> {
    return this.bookModel.find().exec();
  }

  async findById(id: mongoose.Schema.Types.ObjectId): Promise<Book> {
    return this.bookModel.findById(id).exec();
  }

  async create(book: BookInterface): Promise<Book> {
    const createdBook = new this.bookModel(book);
    return createdBook.save();
  }

  async update(
    id: mongoose.Schema.Types.ObjectId,
    book: BookInterface,
  ): Promise<Book> {
    await this.bookModel.findByIdAndUpdate(id, book).exec();
    return this.findById(id);
  }

  async delete(id: mongoose.Schema.Types.ObjectId) {
    await this.bookModel.findByIdAndRemove(id).exec();
  }
}
