import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { AuthorCreateRequest } from '../request/author.request';
import { AuthorService } from '../service/author.service';
import * as mongoose from 'mongoose';

@Controller('/author')
export class AuthorController {
  constructor(private readonly authorService: AuthorService) {}

  @Get('/')
  async getAllAuthor() {
    return await this.authorService.findAll();
  }

  @Get('/:id')
  async getDetailAuthor(@Param('id') id: mongoose.Schema.Types.ObjectId) {
    return await this.authorService.findById(id);
  }

  @Post('/')
  async createAuthor(@Body() body: AuthorCreateRequest) {
    return await this.authorService.create(body);
  }

  @Put('/:id')
  async updateAuthor(
    @Param('id') id: mongoose.Schema.Types.ObjectId,
    @Body() body: AuthorCreateRequest,
  ) {
    return await this.authorService.update(id, body);
  }

  @Delete('/:id')
  async deleteAuthor(@Param('id') id: mongoose.Schema.Types.ObjectId) {
    await this.authorService.delete(id);
    return { message: 'delete author success' };
  }
}
