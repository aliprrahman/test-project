import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { BookCreateRequest } from '../request/book.request';
import { BookService } from '../service/book.service';
import * as mongoose from 'mongoose';
@Controller('/book')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  @Get('/')
  async getAllBook() {
    return await this.bookService.findAll();
  }

  @Get('/:id')
  async getDetailBook(@Param('id') id: mongoose.Schema.Types.ObjectId) {
    return await this.bookService.findById(id);
  }

  @Post('/')
  async createBook(@Body() body: BookCreateRequest) {
    return await this.bookService.create(body);
  }

  @Put('/:id')
  async updateAuthor(
    @Param('id') id: mongoose.Schema.Types.ObjectId,
    @Body() body: BookCreateRequest,
  ) {
    return await this.bookService.update(id, body);
  }

  @Delete('/:id')
  async deleteAuthor(@Param('id') id: mongoose.Schema.Types.ObjectId) {
    await this.bookService.delete(id);
    return { message: 'delete book success' };
  }
}
