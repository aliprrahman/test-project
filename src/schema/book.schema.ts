import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { Author } from './author.schema';

@Schema()
export class Book {
  @Prop({ required: true })
  title: string;

  @Prop({ required: true })
  category: string;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Author',
    autopopulate: true,
  })
  author: Author;
}

export type BookDocument = Book & Document;
export const BookSchema = SchemaFactory.createForClass(Book);
