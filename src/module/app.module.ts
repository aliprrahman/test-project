import { Module } from '@nestjs/common';
import { AppController } from '../controller/app.controller';
import { AuthorController } from '../controller/author.controller';
import { BookController } from '../controller/book.controller';
import { AppService } from '../service/app.service';
import { AuthorService } from '../service/author.service';
import { BookService } from '../service/book.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { Book, BookSchema } from '../schema/book.schema';
import { Author, AuthorSchema } from '../schema/author.schema';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      expandVariables: true,
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('MONGODB_URI'),
        connectionFactory: (connection) => {
          connection.plugin(require('mongoose-autopopulate'));
          return connection;
        },
      }),
      inject: [ConfigService],
    }),
    MongooseModule.forFeature([
      {
        name: Book.name,
        schema: BookSchema,
      },
      {
        name: Author.name,
        schema: AuthorSchema,
      },
    ]),
  ],
  controllers: [AppController, AuthorController, BookController],
  providers: [AppService, AuthorService, BookService],
})
export class AppModule {}
