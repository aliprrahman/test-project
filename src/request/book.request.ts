import { IsString, MinLength } from 'class-validator';

export class BookCreateRequest {
  @IsString()
  @MinLength(3)
  title: string;

  @IsString()
  category: string;

  @IsString()
  author: string;
}
