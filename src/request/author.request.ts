import { IsString, MinLength, IsEmail } from 'class-validator';

export class AuthorCreateRequest {
  @IsString()
  @MinLength(3)
  name: string;

  @IsString()
  @IsEmail()
  email: string;
}
