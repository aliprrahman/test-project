export class AuthorInterface {
  readonly name: string;
  readonly email: string;
}
