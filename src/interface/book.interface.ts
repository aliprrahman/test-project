export class BookInterface {
  readonly title: string;
  readonly category: string;
  readonly author: string;
}
